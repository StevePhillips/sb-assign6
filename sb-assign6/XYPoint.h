//
//  XYPoint.h
//  sb-assign6
//
//  Created by Student on 2014-10-15.
//  Copyright (c) 2014 Student. All rights reserved.
//
#import <Foundation/Foundation.h>
// -----------------------------
@interface XYPoint : NSObject
// --------------------------
@property int x, y;
// Sythesized
-(void) setX: (int)x :(int)y;
@end