//
//  Ellipse.h
//  sb-assign6
//
//  Created by Student on 2014-10-15.
//  Copyright (c) 2014 Student. All rights reserved.
//
#import "Shape.h"
// --------------
@interface Ellipse : Shape
// -----------------------
@property int aRadius, bRadius;
// ----------------------------
-(void) draw;
-(double) degreetorad: (double)degrees;
@end