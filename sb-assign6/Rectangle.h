//
//  Rectangle.h
//  sb-assign6
//
//  Created by Student on 2014-10-15.
//  Copyright (c) 2014 Student. All rights reserved.
//
#import "Shape.h"

@interface Rectangle : Shape

@property int width, height;

-(void) draw;
@end