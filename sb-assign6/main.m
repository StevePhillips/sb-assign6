//
//  main.m
//  sb-assign6
//
//  Created by Student on 2014-10-15.
//  Copyright (c) 2014 Student. All rights reserved.
//

#import "Driver.h"

int main(int argc, const char * argv[])
{
  @autoreleasepool
  {
    [[[Driver alloc] init] run];
  }
  return 0;
}
