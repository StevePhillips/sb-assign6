//
//  Square.h
//  sb-assign6
//
//  Created by Student on 2014-10-15.
//  Copyright (c) 2014 Student. All rights reserved.
//
#import "Rectangle.h"

@interface Square : Rectangle

@property int size;

@end