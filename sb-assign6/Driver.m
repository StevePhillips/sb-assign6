//
//  Driver.m
//  sb-assign6
//
//  Created by Student on 2014-10-15.
//  Copyright (c) 2014 Student. All rights reserved.
//

#import "Driver.h"
#import "Square.h"
#import "Circle.h"
#import "Line.h"
@implementation Driver

-(void) run
{
  NSMutableArray* my_array = [[NSMutableArray alloc] init];
  
  id shape;
  
  for(int counter = 0; counter < 10; counter++)
  {
    int shapetype = rand()%5;
    
    switch(shapetype)
    {
      case 0:
        shape = [[Square alloc] init];
        ((Square*)shape).size = 6;
        
        [my_array addObject:shape];
        break;
      case 1:
        shape = [[Ellipse alloc] init];
        ((Ellipse*)shape).aRadius = rand()%10;
        ((Ellipse*)shape).bRadius = rand()%10;
  
        [my_array addObject:shape];
        break;
      case 2:
        shape = [[Circle alloc] init];
        ((Circle*)shape).size = 20;
  
        [my_array addObject:shape];
        break;
      case 3: {
        shape = [[Line alloc] init];
        Line* ln = shape;
        [ln.second setX: rand()%20:rand()%20];
  
        [my_array addObject:shape];
        break;
      }
      case 4:
        shape = [[Rectangle alloc] init];
        Rectangle* rec = shape;
        rec.width = rand()%15;
        rec.height = rand()%16;
        break;
    }
  }
  
  for(id ashape in my_array)
      [ashape draw];
}

@end