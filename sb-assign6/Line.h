//
//  Line.h
//  sb-assign6
//
//  Created by Student on 2014-10-15.
//  Copyright (c) 2014 Student. All rights reserved.
//
#import "Shape.h"
// --------------
#import "XYPoint.h"

@interface Line : Shape
// --------------------
@property XYPoint* first;
@property XYPoint* second;

-(instancetype) init;
// -----------------------
-(void) draw;
@end