//
//  Circle.m
//  sb-assign6
//
//  Created by Student on 2014-10-15.
//  Copyright (c) 2014 Student. All rights reserved.
//

#import "Circle.h"

@implementation Circle

-(void) setSize: (int)nsize
{
  _size = nsize;
  self.aRadius = nsize;
  self.bRadius = nsize;
}

@end
