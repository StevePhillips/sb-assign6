//
//  Ellipse.m
//  sb-assign6
//
//  Created by Student on 2014-10-15.
//  Copyright (c) 2014 Student. All rights reserved.
//

#import "Ellipse.h"


@implementation Ellipse

-(double) degreetorad: (double)degrees
{
  const double const_pi = 3.1415;
  return degrees*const_pi/180.0;
}
-(void) draw
{
  char raster[50][50];
  
  // Clear the raster for drawing
  for(int itery = 0; itery < 50; itery++)
    for(int iterx = 0; iterx < 50; iterx++)
      raster[itery][iterx] = ' ';
  
  // Using a radius and b radius calculate how far away from the
  // edge of the raster to be
  // for 360 degrees plot a circle
  // These calculations are ones I've used since I was 14
  for(int iter = 0; iter < 360; iter++)
  {
    int x = self.aRadius+(cos([self degreetorad: iter])*self.aRadius);
    int y = self.bRadius+(sin([self degreetorad: iter])*self.bRadius);
    // --------------------
    raster[y][x] = 'O';
  }
  int endHeight = self.bRadius*2+1;
  for(int iter = 0; iter < endHeight; iter++)
  {
    for(int iter2 = 0; iter2 < 50; iter2++)
      putchar(raster[iter][iter2]);
    putchar('\n');
  }
}

@end
