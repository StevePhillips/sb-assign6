//
//  XYPoint.m
//  sb-assign6
//
//  Created by Student on 2014-10-15.
//  Copyright (c) 2014 Student. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XYPoint.h"
// ----------------
@implementation XYPoint

-(void) setX: (int)nx :(int)ny
{
    self.x = nx;
    self.y = ny;
}

@end