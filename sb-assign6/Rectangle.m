//
//  Rectangle.m
//  sb-assign6
//
//  Created by Student on 2014-10-15.
//  Copyright (c) 2014 Student. All rights reserved.
//
#import "Rectangle.h"

@implementation Rectangle

@synthesize width;
@synthesize height;

-(void) draw
{
  for(int iter = 0; iter < width; iter++)
    fputs("--", stdout);
  
  putchar('\n');
  
  for(int iter = 0; iter < height; iter++)
  {
    putchar('|');
    // ----------
    for(int iter2 = 0; iter2 < width-2; iter2++)
      fputs("  ", stdout);
    
    puts("  |"); // \n
  }
  for(int iter = 0; iter < width; iter++)
    fputs("--", stdout);
  
  putchar('\n');
}

@end