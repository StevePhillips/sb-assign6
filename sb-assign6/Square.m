//
//  Square.m
//  sb-assign6
//
//  Created by Student on 2014-10-15.
//  Copyright (c) 2014 Student. All rights reserved.
//

#import "Square.h"

@implementation Square

@synthesize size = _size;

-(int) size
{
  return _size;
}

-(void) setSize:(int)nsize
{
  _size = nsize;
  self.width = nsize;
  self.height = nsize;
}

@end
