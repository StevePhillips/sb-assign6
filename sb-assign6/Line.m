//
//  Line.m
//  sb-assign6
//
//  Created by Student on 2014-10-15.
//  Copyright (c) 2014 Student. All rights reserved.
//

#import "Line.h"
#import "XYPoint.h"

@implementation Line
@synthesize first;
@synthesize second;

-(double) slope: (XYPoint*) left :(XYPoint*) right
{
  double rise = right.y - left.y;
  double run = right.x - left.x;
  
  return rise / run;
}

-(instancetype) init
{
  self = [super init];
  first = [[XYPoint alloc] init];
  first.x = 0;
  first.y = 0;
  second = [[XYPoint alloc] init];
  second.x = 0;
  second.y = 0;
  
  return self;
}

-(void) draw
{
  char raster[50][50];
  
  // Clear the raster for drawing
  for(int itery = 0; itery < 50; itery++)
    for(int iterx = 0; iterx < 50; iterx++)
      raster[itery][iterx] = ' ';
  
  XYPoint* left;
  XYPoint* right;
  
  if(first.x < second.x)
  {
    left = first;
    right = second;
  }
  else
  {
    right = first;
    left = second;
  }
  double inc = [self slope:left :right];
  
  double current_y = left.y;
  
  // rasterize
  for(int x = left.x; x <= right.x; x++)
  {
    raster[(int)current_y][x] = '#';
    current_y += inc;
  }
  int max_height;
  
  if(left.y > right.y)
    max_height = left.y;
  else
    max_height = right.y;
  
  // render
  for(int itery = 0; itery <= max_height; itery++)
  {
    for(int iterx = 0; iterx < 50; iterx++)
      putchar(raster[itery][iterx]);
    putchar('\n');
  }
  
}

@end
